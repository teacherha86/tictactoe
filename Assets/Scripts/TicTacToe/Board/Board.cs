using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TicTacToePlayers
{
    None,
    X,
    O
}

public class Board : MonoBehaviour
{
    [Header("Board Grid")]
    public int MaxX = 3;
    public int MaxY = 3;

    [Header("Cells")]
    [SerializeField] private GameObject cellPrefab;
    [SerializeField] private float cellGap;

    private BoardCell[,] cells;
    public void InitializeBoard()
    {
        MakeCells();
    }

    void MakeCells ()
    {
        cells = new BoardCell[MaxY, MaxX];
        // Move from LT to RB
        Vector3 startPos = transform.position + new Vector3(-MaxX * 0.5f, MaxY * 0.5f, 0);        
        for(int y = 0; y < MaxX; y++)
        {
            for(int x = 0; x < MaxX; x++)
            {
                int index = y * MaxX + x;
                Vector3 newPos = startPos + new Vector3(cellGap * x, -cellGap * y);
                var cellGo = Instantiate(cellPrefab, transform);
                cellGo.transform.position = newPos;
                cellGo.name = index.ToString();
                cellGo.GetComponent<BoardCell>().Index = index;
                cells[y, x] = cellGo.GetComponent<BoardCell>();
            }
        }
    }


    public void Select(int x, int y, TicTacToePlayers player)
    {
        cells[y, x].Select(player);
    }


}
