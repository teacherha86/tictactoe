using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardCell : MonoBehaviour
{  
    public int Index = -1;
    public TicTacToePlayers selectedState = TicTacToePlayers.None;

#if UNITY_EDITOR
    [ContextMenu("Select X")]
    void SelectX()
    {
        Select(TicTacToePlayers.X);
    }

    [ContextMenu("Select O")]
    void SelectO()
    {
        Select(TicTacToePlayers.O);
    }
#endif

    public void Select (TicTacToePlayers state)
    {
        this.selectedState = state;

        switch (state)
        {
            case TicTacToePlayers.X:
                GetComponentInChildren<MeshRenderer>().material.color = Color.red;
                break;
            case TicTacToePlayers.O:
                GetComponentInChildren<MeshRenderer>().material.color = Color.green;
                break;
        }

    }

}
