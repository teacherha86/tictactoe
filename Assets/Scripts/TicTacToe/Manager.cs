using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public Board board;

    TicTacToePlayers playingPlayer;
    
    void Start()
    {
        playingPlayer = TicTacToePlayers.X;
        // you have to call initialize board function
        board.InitializeBoard();      
        

        // turn base rule...
    }

    void ChangePlayer()
    {
        if (playingPlayer == TicTacToePlayers.X)
            playingPlayer = TicTacToePlayers.O;
        else
            playingPlayer = TicTacToePlayers.X;
    }

    public void OnSelectCell(int x, int y, TicTacToePlayers player)
    {
        //board.Select(0, 0, TicTacToePlayers.X);
        //board.Select(1, 1, TicTacToePlayers.O);

        //board.Select(2, 0, TicTacToePlayers.X);
        //board.Select(3, 1, TicTacToePlayers.O);
        board.Select(x, y, player);

        // check who is win....
        // else not win, turn change...
        ChangePlayer();
    }
}
